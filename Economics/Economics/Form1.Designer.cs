﻿namespace Economics
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.output_lb_1 = new System.Windows.Forms.Label();
            this.output_lb_2 = new System.Windows.Forms.Label();
            this.output_lb_3 = new System.Windows.Forms.Label();
            this.output_lb_4 = new System.Windows.Forms.Label();
            this.calc_btn = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(9, 25);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(148, 20);
            this.textBox1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Кількість рядків коду(тисяч)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Трудоємність";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Тривалість";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 169);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Розробників";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 209);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Продуктивність";
            // 
            // output_lb_1
            // 
            this.output_lb_1.AutoSize = true;
            this.output_lb_1.Location = new System.Drawing.Point(125, 94);
            this.output_lb_1.Name = "output_lb_1";
            this.output_lb_1.Size = new System.Drawing.Size(0, 13);
            this.output_lb_1.TabIndex = 6;
            // 
            // output_lb_2
            // 
            this.output_lb_2.AutoSize = true;
            this.output_lb_2.Location = new System.Drawing.Point(125, 133);
            this.output_lb_2.Name = "output_lb_2";
            this.output_lb_2.Size = new System.Drawing.Size(0, 13);
            this.output_lb_2.TabIndex = 7;
            // 
            // output_lb_3
            // 
            this.output_lb_3.AutoSize = true;
            this.output_lb_3.Location = new System.Drawing.Point(125, 169);
            this.output_lb_3.Name = "output_lb_3";
            this.output_lb_3.Size = new System.Drawing.Size(0, 13);
            this.output_lb_3.TabIndex = 8;
            // 
            // output_lb_4
            // 
            this.output_lb_4.AutoSize = true;
            this.output_lb_4.Location = new System.Drawing.Point(125, 209);
            this.output_lb_4.Name = "output_lb_4";
            this.output_lb_4.Size = new System.Drawing.Size(0, 13);
            this.output_lb_4.TabIndex = 9;
            // 
            // calc_btn
            // 
            this.calc_btn.Location = new System.Drawing.Point(237, 239);
            this.calc_btn.Name = "calc_btn";
            this.calc_btn.Size = new System.Drawing.Size(94, 23);
            this.calc_btn.TabIndex = 10;
            this.calc_btn.Text = "Розрахувати";
            this.calc_btn.UseVisualStyleBackColor = true;
            this.calc_btn.Click += new System.EventHandler(this.calc_btn_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Розповсюджений ",
            "Напівнезалежний",
            "Вбудований"});
            this.comboBox1.Location = new System.Drawing.Point(15, 274);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 249);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Тип";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(363, 410);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.calc_btn);
            this.Controls.Add(this.output_lb_4);
            this.Controls.Add(this.output_lb_3);
            this.Controls.Add(this.output_lb_2);
            this.Controls.Add(this.output_lb_1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label output_lb_1;
        private System.Windows.Forms.Label output_lb_2;
        private System.Windows.Forms.Label output_lb_3;
        private System.Windows.Forms.Label output_lb_4;
        private System.Windows.Forms.Button calc_btn;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label6;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Economics
{
   
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
       
        

        private void calc_btn_Click(object sender, EventArgs e)
        {
            output_lb_1.Text = null;
            output_lb_2.Text = null;
            output_lb_3.Text = null;
            output_lb_4.Text = null;

            double pm = 0.0;
            double tm = 0.0;
            double ss = 0.0;
            double p = 0.0;

            int n;
            double[,] modelTable = new double[3, 4]{                { 2.4, 1.05, 2.5, 0.38 },
                { 3.0, 1.12, 2.5, 0.35 },
                { 3.6, 1.20, 2.5, 0.32 }
            };
            if (!int.TryParse(textBox1.Text, out n)) {
                MessageBox.Show("Введеные данные неверны") ;
                output_lb_1.Text = null;
                output_lb_2.Text = null;
                output_lb_3.Text = null;
                output_lb_4.Text = null;
                return;
            }

          if (comboBox1.SelectedIndex == 0)
            {
                pm = modelTable[0, 0] * Math.Pow(n,modelTable[0,1]);
                tm = modelTable[0,2] * Math.Pow(pm, modelTable[0,3]);
                ss = pm/tm;
                p = n/pm;
                pm = Math.Round(pm, 2);
                tm = Math.Round(tm, 2);
                ss = Math.Round(ss, 2);
                p = Math.Round(p, 2);
                output_lb_1.Text += pm.ToString();
                output_lb_2.Text += tm.ToString();
                output_lb_3.Text += ss.ToString();
                output_lb_4.Text += p.ToString();

              
            }
            else
            
                
             if (comboBox1.SelectedIndex == 1 &&  n < 301)
            {
                pm = modelTable[1, 0] * Math.Pow(n,modelTable[1,1]);
                tm = modelTable[1,2] * Math.Pow(pm, modelTable[1,3]);
                ss = pm/tm;
                p = n/pm;
                pm = Math.Round(pm,2);
                tm = Math.Round(tm, 2);
                ss = Math.Round(ss, 2);
                p = Math.Round(p, 2);
                output_lb_1.Text += pm.ToString();
                output_lb_2.Text += tm.ToString();
                output_lb_3.Text += ss.ToString();
                output_lb_4.Text += p.ToString();
                
            }
             else
            if (comboBox1.SelectedIndex == 2)
            {
                pm = modelTable[2,0] * Math.Pow(n, modelTable[2,1]);
                tm = modelTable[2,2] * Math.Pow(pm, modelTable[2,3]);
                ss = pm/tm;
                p = n/pm;
                pm = Math.Round(pm, 2);
                tm = Math.Round(tm, 2);
                ss = Math.Round(ss, 2);
                p = Math.Round(p, 2);
                output_lb_1.Text += pm.ToString();
                output_lb_2.Text += tm.ToString();
                output_lb_3.Text += ss.ToString();
                output_lb_4.Text += p.ToString();
                
            }
            
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }


        

    }
}
